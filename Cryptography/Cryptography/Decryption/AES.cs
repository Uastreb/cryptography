﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace MySecurityLib.Decryption
{
    public static class AES
    {
        static string readerProtocol(byte[] cipherText, ICryptoTransform decryptor)
        {
            string plainText;
            using (System.IO.MemoryStream msDecrypt = new System.IO.MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (System.IO.StreamReader srDecrypt = new System.IO.StreamReader(csDecrypt))
                    {
                        plainText = srDecrypt.ReadToEnd();
                    }
                }
            }
            return plainText;
        }

        public static string Decrypt(byte[] cipherText, byte[] Key, byte[] IV)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            var myRijndael = new RijndaelManaged();
            myRijndael.Key = Key;
            myRijndael.IV = IV;

            ICryptoTransform decryptor = myRijndael.CreateDecryptor(myRijndael.Key, myRijndael.IV);

            string plainText = readerProtocol(cipherText, decryptor);

            return plainText;
        }

        public static string Decrypt(byte[] cipherText, byte[] Key)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");

            var myRijndael = new RijndaelManaged();
            myRijndael.Mode = CipherMode.ECB;
            myRijndael.BlockSize = 128;
            myRijndael.Padding = PaddingMode.Zeros;
            myRijndael.Key = Key;

            ICryptoTransform decryptor = myRijndael.CreateDecryptor(myRijndael.Key, null);

            string plainText = readerProtocol(cipherText, decryptor);

            return plainText;
        }
    }
}
