﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace MySecurityLib.Decryption
{
    public static class RSA
    {
        public static string Decrypt(byte[] cipherText, RSAParameters rsaParameters)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(rsaParameters);
            byte[] getData = rsa.Decrypt(cipherText, true);

            return Convert.ToString(Encoding.UTF8.GetString(getData));
        }
    }
}
