﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace MySecurityLib.Encryption
{
    public static class AES
    {
        static byte[] writerProtocol(string plainText, ICryptoTransform encryptor)
        {
            byte[] cipherText;

            using (System.IO.MemoryStream msEncrypt = new System.IO.MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (System.IO.StreamWriter swEncrypt = new System.IO.StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(plainText);
                    }
                    cipherText = msEncrypt.ToArray();
                }
            }
            return cipherText;
        }

        public static class CBC
        {
            public static (byte[] cipherText, byte[] Key, byte[] IV) Encrypt(string plainText)
            {
                var myRijndael = new RijndaelManaged();
                myRijndael.Mode = CipherMode.CBC;
                myRijndael.GenerateKey();
                myRijndael.GenerateIV();

                ICryptoTransform encryptor = myRijndael.CreateEncryptor(myRijndael.Key, myRijndael.IV);

                var cipherText = writerProtocol(plainText, encryptor);

                return (cipherText: cipherText, Key: myRijndael.Key, IV: myRijndael.IV);
            }
        }

        public static class ECB
        {
            public static (byte[] cipherText, byte[] Key) Encrypt(string plainText)
            {
                var myRijndael = new RijndaelManaged();
                myRijndael.Mode = CipherMode.ECB;
                myRijndael.BlockSize = 128;
                myRijndael.Padding = PaddingMode.Zeros;
                myRijndael.GenerateKey();

                ICryptoTransform encryptor = myRijndael.CreateEncryptor(myRijndael.Key, null);

                var cipherText = writerProtocol(plainText, encryptor);

                return (cipherText: cipherText, Key: myRijndael.Key);
            }
        }
    }
}
