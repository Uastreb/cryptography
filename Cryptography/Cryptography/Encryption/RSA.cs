﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace MySecurityLib.Encryption
{
    public static class RSA
    {
        public static (byte[] cipherText, RSAParameters rsaParameters) Encrypt(string plainText)
        {
            var rsa = new RSACryptoServiceProvider();
            var rsaParametres = rsa.ExportParameters(true);

            byte[] cipherText = rsa.Encrypt(Encoding.UTF8.GetBytes(plainText), true);

            return (cipherText: cipherText, rsaParameters: rsaParametres);
        }
    }
}
