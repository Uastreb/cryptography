﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySecurityLib.Password
{
    public static class HashedPassword
    {
        public static (string dynamicSalt, string hashedPassword) GetHashedPassword(string password, string staticSalt, string algHash = "sha1")
        {
            var dynamicSalt = Salt.Salting.GetSalt();
            string hashedPasswordWithDynamicSalt, hashedPasswordWithStaticAndDynamicSalt;
            switch (algHash)
            {
                case "md5":
                    hashedPasswordWithDynamicSalt = Hash.MD5.GetMD5(password + dynamicSalt);
                    hashedPasswordWithStaticAndDynamicSalt = Hash.MD5.GetMD5(hashedPasswordWithDynamicSalt + staticSalt);
                    break;
                case "sha1":
                    hashedPasswordWithDynamicSalt = Hash.SHA.GetSHA1(password + dynamicSalt);
                    hashedPasswordWithStaticAndDynamicSalt = Hash.SHA.GetSHA1(hashedPasswordWithDynamicSalt + staticSalt);
                    break;
                case "sha256":
                    hashedPasswordWithDynamicSalt = Hash.SHA.GetSHA256(password + dynamicSalt);
                    hashedPasswordWithStaticAndDynamicSalt = Hash.SHA.GetSHA256(hashedPasswordWithDynamicSalt + staticSalt);
                    break;
                case "sha384":
                    hashedPasswordWithDynamicSalt = Hash.SHA.GetSHA384(password + dynamicSalt);
                    hashedPasswordWithStaticAndDynamicSalt = Hash.SHA.GetSHA384(hashedPasswordWithDynamicSalt + staticSalt);
                    break;
                case "sha512":
                    hashedPasswordWithDynamicSalt = Hash.SHA.GetSHA512(password + dynamicSalt);
                    hashedPasswordWithStaticAndDynamicSalt = Hash.SHA.GetSHA512(hashedPasswordWithDynamicSalt + staticSalt);
                    break;
                default:
                    hashedPasswordWithDynamicSalt = Hash.SHA.GetSHA1(password + dynamicSalt);
                    hashedPasswordWithStaticAndDynamicSalt = Hash.SHA.GetSHA1(hashedPasswordWithDynamicSalt + staticSalt);
                    break;
            }

            return (dynamicSalt: dynamicSalt, hashedPassword: hashedPasswordWithStaticAndDynamicSalt);
        }
    }
}
