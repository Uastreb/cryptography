﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySecurityLib.Salt
{
    public static class Salting
    {
        public static string GetSalt(int n = 16)
        {
            var salt = System.Web.Helpers.Crypto.GenerateSalt(n);
            return salt;
        }
    }
}
