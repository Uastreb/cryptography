﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace TestCrypto
{
    class Program
    {
        class User
        {
            public int id { get; set; }
            public string fullName { get; set; }
            public string mail { get; set; }
            public DateTime dateOfBith { get; set; }
            public string password { get; set; }
            public string phone { get; set; }
            public string dynamicSalt { get; set; }
        }

        static string OutputArrBytes(byte[] mas)
        {
            StringBuilder text = new StringBuilder();
            foreach (var a in mas)
            {
                text.Append($"{a}  ");
            }
            return text.ToString();
        }

        static void Main(string[] args)
        {
            User user = new User();

            user.id = 0;
            user.fullName = "Иванов И.И.";
            user.dateOfBith = DateTime.Parse("10.10.1997");
            user.phone = "+ 375 29 123 45 67";
            user.mail = "Ivanov.Proger@mail.ru";
            user.password = "qwerty";
            user.dynamicSalt = "";

            {
                WriteLine($"Получим хэш от пароля пользователя, алгоритм хеширования SHA1 \nЭто будет:{MySecurityLib.Hash.SHA.GetSHA1(user.password)}");
                WriteLine($"Получим хэш от пароля пользователя, алгоритм хеширования MD5 \nЭто будет:{MySecurityLib.Hash.MD5.GetMD5(user.password)}");
            }

            {
                WriteLine($"\n\nЗашифруем ФИО пользователя шифром AES с вектором инициализации");
                (byte[] cipherText, byte[] key, byte[] IV) = MySecurityLib.Encryption.AES.CBC.Encrypt(user.fullName);
                WriteLine($"Полученные данные: {OutputArrBytes(cipherText)} , \nКлюч: {OutputArrBytes(key)} , \nВектор инициализации: {OutputArrBytes(IV)} ");
                string plainText = MySecurityLib.Decryption.AES.Decrypt(cipherText, key, IV);
                WriteLine($"Расшифровываем зашифрованное сообщение \nПолученное сообщение: '{plainText}'");

            }

            {
                WriteLine($"\n\nЗашифруем ФИО пользователя шифром AES без вектора инициализации");
                (byte[] cipherText, byte[] key) = MySecurityLib.Encryption.AES.ECB.Encrypt(user.fullName);
                WriteLine($"Полученные данные: {OutputArrBytes(cipherText)} , \nКлюч: {OutputArrBytes(key)}");
                string plainText = MySecurityLib.Decryption.AES.Decrypt(cipherText, key);
                WriteLine($"Расшифровываем зашифрованное сообщение \nПолученное сообщение: '{plainText}'");

            }


            {
                WriteLine($"\n\nЗашифруем ФИО пользователя шифром RSA");
                (byte[] cipherText, System.Security.Cryptography.RSAParameters rsaParametres) = MySecurityLib.Encryption.RSA.Encrypt(user.fullName);
                WriteLine($"Полученные данные: {OutputArrBytes(cipherText)}");
                string plainText = MySecurityLib.Decryption.RSA.Decrypt(cipherText, rsaParametres);
                WriteLine($"Расшифровываем зашифрованное сообщение \nПолученное сообщение: '{plainText}'");
            }

            {
                WriteLine($"\n\nСоздадим соль:{MySecurityLib.Salt.Salting.GetSalt()}");
            }

            {
                const string staticSalt = "tZnXS/hYAKzzertS20LbmA==";
                WriteLine("Получим хеш пароля пользователя с применением солей к ему для обеспечения доп. защиты");
                (user.dynamicSalt, user.password) = MySecurityLib.Password.HashedPassword.GetHashedPassword("qwerty", staticSalt, "sha256");
                WriteLine($"Получаем: статическая соль: {staticSalt} \nДинамическая соль: {user.dynamicSalt} \nХеш пароля: {user.password}");
            }

            ReadLine();
        }
    }
    }
